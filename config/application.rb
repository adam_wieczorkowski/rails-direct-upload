require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RailsDirectUpload
  class Application < Rails::Application
    require 'firefield_formatter'

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.active_job.queue_adapter = :delayed_job

    # Custom logs formatter (to use it remove rails_semantic_logger gem)
    #config.log_formatter = FirefieldFormatter.new

    # Semantic Logger config
    #config.semantic_logger.backtrace_level = :info
    #config.rails_semantic_logger.ap_options = { multiline: true }
  end
end
