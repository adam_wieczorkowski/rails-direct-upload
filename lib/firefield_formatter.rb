class FirefieldFormatter < ActiveSupport::Logger::SimpleFormatter
  def call(severity, timestamp, progname, message)
    {
      type: severity,
      time: timestamp,
      message: message
    }.to_json + "\r\n"
  end
end
