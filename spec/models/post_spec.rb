require 'rails_helper'

describe Post do
  describe 'associations' do
    it { is_expected.to have_many(:post_images) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:title) }
  end
end
