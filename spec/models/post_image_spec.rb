require 'rails_helper'

describe PostImage do
  describe 'associations' do
    it { is_expected.to belong_to(:post) }
  end

  describe 'validations' do
    subject { create :post_image }

    it { is_expected.to validate_presence_of(:post) }
    it { is_expected.to validate_presence_of(:name) }
  end
end
