require 'rails_helper'

describe CreatePostImage do
  let(:post) { create :post }
  subject { described_class.call(post, create_params) }

  context 'with valid create_params' do
    let(:create_params) { { 
      name: 'Bullet',
      key: "uploads/post_#{post.id}/post_image/53e94f56-63ad-49fc-a0e4-8202c8fe58b9/Dafty.png"
    } }

    it 'should create new PostImage' do
      expect{ subject }.to change{ PostImage.count }.by(1)
    end

    it 'should add new job' do
      expect{ subject }.to change{ Delayed::Job.count }.by(1)
    end
  end

  context 'with invalid create_params' do
    let(:create_params) { { name: '' } }

    it 'should not create new PostImage' do
      expect{ subject }.to_not change{ PostImage.count }
    end

    it 'should not add new job' do
      expect{ subject }.to_not change{ Delayed::Job.count }
    end
  end
end
