# Direct Upload Test App

## Requirements

```
Ruby: 2.4.0
```

## Running

1. copy config/application.yml.sample to config/application.yml and set valid
value to each key

2. Install all the required gems with `bundle install` command.

3. Create database with `rake db:setup` command.

4. Run rails server: `rails s`

5. After all don't forget to run background jobs with `rails jobs:work`
