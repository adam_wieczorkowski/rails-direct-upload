class AddProcessedToPostImages < ActiveRecord::Migration[5.0]
  def change
    add_column :post_images, :processed, :boolean, default: false, null: false
  end
end
