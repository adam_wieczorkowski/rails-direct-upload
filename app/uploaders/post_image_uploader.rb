class PostImageUploader < CarrierWave::Uploader::Base
  include CarrierWaveDirect::Uploader
  include CarrierWave::MiniMagick

  def store_dir
    "uploads/post_#{model.post.id}/#{model.class.to_s.underscore}"
  end

  version :thumb do
    process resize_to_limit: [100, 100]
  end

  version :medium do
    process resize_to_fill: [300, 300]
  end

  version :large do
    process resize_to_fill: [400, 600]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
end
