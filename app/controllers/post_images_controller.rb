class PostImagesController < ApplicationController
  def new
    @post_image = post.post_images.new(key: params[:key])
  end

  def create
    if CreatePostImage.call(post, image_params)
      redirect_to post_path(post)
      flash[:notice] = "Image was successfully added. Now it will be processed."
    else
      render action: :new
    end
  end

  private

  def post
    @post ||= Post.find(params[:post_id])
  end

  def image_params
    params.require(:post_image).permit(:name, :key)
  end
end
