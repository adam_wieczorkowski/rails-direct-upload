class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def show
    @post = Post.find(params[:id])
    @uploader = PostImage.new(post: @post).image
    @uploader.success_action_redirect = new_post_post_image_url(@post)
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.create(post_params)
    if @post.persisted?
      redirect_to action: :index
    else
      render action: :new
    end
  end

  private

  def post_params
    params.require(:post).permit(:title)
  end
end
