class CreatePostImage < BaseService
  attr_accessor :post, :create_params

  def initialize(post, create_params)
    self.post = post
    self.create_params = create_params
  end

  def call
    post_image = post.post_images.create(create_params)
    if post_image.persisted?
      PostImageProcessorJob.perform_later(post_image.id)
    end
  end
end
