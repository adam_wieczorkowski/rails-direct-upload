module ApplicationHelper
  def post_image_url(post_image, size)
    if post_image.processed?
      post_image.image.public_send(size)
    else
      "/#{size}_placeholder.png"
    end
  end
end
